#include <stdio.h>

#include "question_2.h"

#define CAPACITY_A 30
#define CAPACITY_B 70
#define MAX_VALUE 127

#define FILE_NAME "textfile.txt"

void format_print(const char *, sparse_set_ptr);

int main(void)
{
    /******************************************************************
     *  Question 2D(i)
     ******************************************************************/

    char *string_a = "The quick brown fox jumps over the lazy dog.";
    char *string_b = "Jack and Jill went up the hill\n347 times!!!";

    sparse_set_ptr sparse_set_A = ss_init(CAPACITY_A, MAX_VALUE);
    sparse_set_ptr sparse_set_B = ss_init(CAPACITY_B, MAX_VALUE);

    ss_populate_from_text(string_a, sparse_set_A);
    ss_populate_from_text(string_b, sparse_set_B);

    format_print("Sparse Set A", sparse_set_A);
    format_print("Sparse Set B", sparse_set_B);

    ssize_t size_A = ss_size(sparse_set_A);
    ssize_t size_B = ss_size(sparse_set_B);

    /******************************************************************
     *  Question 2D(ii)
     ******************************************************************/

    sparse_set_index_t capacity_union;
    sparse_set_index_t total_size = size_A + size_B;

    if(total_size < MAX_VALUE+1)
        capacity_union = total_size;
    else
        capacity_union = MAX_VALUE+1;

    sparse_set_ptr union_sparse_set = ss_init(capacity_union, MAX_VALUE);

    if(ss_union(sparse_set_A, sparse_set_B, union_sparse_set))
        format_print("Union of A and B", union_sparse_set);
    else
        fputs("\nCouldn't perform union.\n", stderr);

    /******************************************************************
      *  Question 2D(iii)
      ******************************************************************/

    sparse_set_index_t capacity_intersection;

    if(size_A < size_B)
        capacity_intersection = size_A;
    else
        capacity_intersection = size_B;

    sparse_set_ptr intersection_sparse_set = ss_init(capacity_intersection, MAX_VALUE);

    if(ss_intersection(sparse_set_A, sparse_set_B, intersection_sparse_set))
        format_print("Intersection of A and B", intersection_sparse_set);
    else
        fputs("\nCouldn't perform intersection.\n", stderr);

    /******************************************************************
     *  Question 2D(iv)
     ******************************************************************/

    sparse_set_ptr difference_sparse_set = ss_init(size_A, MAX_VALUE);

    if(ss_difference(sparse_set_A, sparse_set_B, difference_sparse_set))
        format_print("Set Difference of B from A (A-B)", difference_sparse_set);
    else
        fputs("\nCouldn't perform set difference.\n", stderr);

    ss_destroy(sparse_set_A);
    ss_destroy(sparse_set_B);
    ss_destroy(union_sparse_set);
    ss_destroy(intersection_sparse_set);
    ss_destroy(difference_sparse_set);

    /******************************************************************
     *  Question 2D(v)
     ******************************************************************/

    sparse_set_ptr result = ss_init(MAX_VALUE+1, MAX_VALUE);

    if(ss_populate_from_file(FILE_NAME, result))
        format_print("Sparse Set populated from file", result);

    ss_destroy(result);

    return 0;
}

/**
 * Displays both values and characters of a sparse set, along with a label
 * @param label         description of the sparse set being displayed
 * @param sparse_set    the sparse set whose values are to be displayed
 */
void format_print(const char *label, sparse_set_ptr sparse_set)
{
    printf("\n%s\n", label);
    if(ss_size(sparse_set) != 0)
    {
        ss_print_status(sparse_set, false);
        ss_print_status(sparse_set, true);
    }
    else
        ss_print_status(sparse_set, false);
}