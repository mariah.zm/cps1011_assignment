#include <stdlib.h>
#include "question_2.h"
#include <string.h>

/**
 * Question 2A
 */

sparse_set_ptr ss_init(sparse_set_index_t capacity, sparse_set_elem_t max_value)
{
    if(capacity > max_value+1)
    {
        printf("Capacity cannot be greater than the maximum value.\n");
        return NULL;
    }

    sparse_set_ptr sparse_set = (sparse_set_ptr) malloc(sizeof (*sparse_set));

    // Checking if memory was allocated properly
    if(sparse_set == NULL)
    {
        fputs("Couldn't allocate memory.\n", stderr);
        return NULL;
    }

    sparse_set->capacity = capacity;
    sparse_set->max_value = max_value;
    sparse_set->dense_array = (sparse_set_elem_ptr) malloc((sizeof (sparse_set->dense_array))*capacity);
    sparse_set->sparse_array = (sparse_set_index_ptr) malloc((sizeof (sparse_set->sparse_array))*(max_value+1));

    // Checking if memory was allocated properly
    if(sparse_set->dense_array == NULL || sparse_set->sparse_array == NULL)
    {
        fputs("Couldn't allocate memory properly.\n", stderr);
        ss_destroy(sparse_set);
    }

    // On Ubuntu, all elements are initialised to 0. These lines ensure the first 0 in the sparse set
    // won't be considered as part of the sparse set unless added directly by the user
    sparse_set->sparse_array[0] = 1;
    sparse_set->dense_array[1] = 1;

    return sparse_set;
}

bool ss_destroy(sparse_set_ptr sparse_set)
{
    // If given ptr is null, no memory to deallocate
    if(sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return false;
    }

    free(sparse_set);
    sparse_set = NULL;

    return true;
}

bool ss_add(sparse_set_ptr sparse_set, sparse_set_elem_t element)
{
    // If given ptr is null, cannot do anything
    if(sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return false;
    }

    sparse_set_index_t next_index = ss_size(sparse_set);

    // If the sparse set is full, we cannot add an element
    if(next_index == sparse_set->capacity)
    {
        printf("Sparse set is full.\n");
        return false;
    }

    // If the element given is not in range, it cannot be added
    if(element > sparse_set->max_value || element < 0)
    {
        printf("Given value %d is out of range.\n", element);
        return false;
    }

    // Checking if item already exists
    if(ss_contained(sparse_set, element))
        return true;

    sparse_set->dense_array[next_index] = element;
    sparse_set->sparse_array[element] = next_index;

    return true;
}

bool ss_remove(sparse_set_ptr sparse_set, sparse_set_elem_t element) {
    // If given ptr is null, cannot do anything
    if (sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return false;
    }

    ssize_t size = ss_size(sparse_set);

    // If the sparse set is empty, there is no element to remove
    if (size == 0)
    {
        printf("Given sparse set is empty.\n");
        return false;
    }

    // If the element given is not in range, it doesn't exist in our set so it cannot be removed
    if(element > sparse_set->max_value || element < 0)
    {
        printf("Given value is out of range.\n");
        return false;
    }

    // If the element doesn't exist, it cannot be removed
    if(!ss_contained(sparse_set, element))
    {
        printf("Given value does not exist in given sparse set.\n");
        return false;
    }

    //Finding index of last element
    sparse_set_index_t last_elem_index = size-1;

    // Index of element to be removed
    sparse_set_index_t remove_elem_index = sparse_set->sparse_array[element];

    sparse_set_elem_t last_elem = sparse_set->dense_array[last_elem_index];

    sparse_set->dense_array[remove_elem_index] = last_elem;
    sparse_set->sparse_array[last_elem] = remove_elem_index;

    return true;
}

bool ss_contained(sparse_set_ptr sparse_set, sparse_set_elem_t element)
{
    // If given ptr is null, cannot do anything
    if(sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return false;
    }

    // If the element given is not in range, it definitely isn't contained
    if(element > sparse_set->max_value || element < 0)
    {
        printf("Given value is out of range.\n");
        return false;
    }

    sparse_set_index_t index_to_check = sparse_set->sparse_array[element];

    // Doing bounds checking prior to looking at value at index
    if(index_to_check <= sparse_set->max_value && index_to_check >= 0
            && sparse_set->dense_array[index_to_check] == element)
        return true;
    else
        return false;
}

bool ss_sort(sparse_set_ptr sparse_set)
{
    // If given ptr is null, cannot do anything
    if(sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return false;
    }

    ssize_t size = ss_size(sparse_set);

    if(size == 0)
    {
        printf("Cannot sort given sparse set as it is empty.\n");
        return false;
    }

    // Selection sort
    sparse_set_index_t index, next_index, min_index;

    // One by one move boundary of unsorted subarray
    for (index = 0; index < size - 1; index++)
    {
        // Find the minimum element
        min_index = index;
        for (next_index = index + 1; next_index < size; next_index++)
            if (sparse_set->dense_array[next_index] < sparse_set->dense_array[min_index])
                min_index = next_index;

        // Swapping minimum element with first element, and updating indices in sparse_array
        sparse_set_elem_t temp_elem = sparse_set->dense_array[index];
        sparse_set_elem_t min_elem = sparse_set->dense_array[min_index];

        sparse_set->dense_array[index] = min_elem;
        sparse_set->sparse_array[min_elem] = index;

        sparse_set->dense_array[min_index] = temp_elem;
        sparse_set->sparse_array[temp_elem] = min_index;
    }

    return true;
}

ssize_t ss_size(sparse_set_ptr sparse_set)
{
    // If given ptr is null, cannot calculate size
    if(sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return -1;
    }

    // Variables used to check if elements in dense_array at the indices pointed to by sparse_array match
    sparse_set_elem_t elem_to_check;
    sparse_set_index_t index = 0;

    size_t size = 0;

//    //If the first two elements are both zero, it means there are no elements in the set (uniqueness property)
//    if(sparse_set->dense_array[0] == 0 && sparse_set->dense_array[1] == 0) return size;


    // Comparing sparse_array with dense_array and incrementing size whenever we get a match
    while(index <= sparse_set->capacity)
    {
        elem_to_check = sparse_set->dense_array[index];

        // Bounds checking
        if(elem_to_check >= 0 && elem_to_check <= sparse_set->max_value)
        {
            // If they don't match, it means we have reached the end of valid elements so we break
            if (sparse_set->sparse_array[elem_to_check] != index)
                break;

            size++;
            index++;
        } else
            break;
    }

    return size;
}

bool ss_get_elem(sparse_set_ptr sparse_set, sparse_set_index_t sparse_index, sparse_set_elem_ptr dest)
{
    // If given ptr is null, cannot get element
    if(sparse_set == NULL)
    {
        printf("Given pointer to sparse set is NULL.\n");
        return false;
    }

    if(sparse_index >= sparse_set->capacity || sparse_index < 0)
    {
        printf("Given index is out of range.\n");
        return false;
    }

    if(sparse_index >= ss_size(sparse_set))
    {
        printf("There is no value stored in that index.\n");
        return false;
    }

    *dest = sparse_set->dense_array[sparse_index];

    return true;
}

bool ss_print_status(sparse_set_ptr sparse_set, bool print_as_chars)
{
    // If given ptr is null, cannot print
    if(sparse_set == NULL)
    {
        printf("Given pointer is NULL.\n");
        return false;
    }

    // If there are no elements, there is nothing to print
    if(ss_size(sparse_set) == 0)
    {
        printf("Empty Sparse Set.\n");
        return true;
    }

    if(print_as_chars)
    {
        printf("Chars:  ");
        SS_FOREACH(elem, sparse_set, printf("%-3c ", elem);)
    } else {
        printf("Values: ");
        SS_FOREACH(elem, sparse_set, printf("%-3d ", elem);)
    }

    printf("\n");

    return true;
}

/**
 * Question 2C
 */

bool ss_union(sparse_set_ptr a, sparse_set_ptr b, sparse_set_ptr result)
{
    // Checking if pointers to sparse sets exist
    if(a == NULL || b == NULL || result == NULL)
    {
        printf("\nOne, or more, of the given pointers is NULL.\n");
        return false;
    }

    if(ss_size(result) != 0)
    {
        printf("\nDestination Sparse Set is not empty!\n");
        return false;
    }

    // Adding all elements found in a
    SS_FOREACH(elem, a,
                   if (!ss_add(result, elem)) {
                       printf("\nError adding %d.\n", elem);
                   })

    // Adding elements found in b - common elements will be handled by the ss_add method
    SS_FOREACH(elem, b,
               if (!ss_add(result, elem)) {
                   printf("\nError adding %d.\n", elem);
               })

    return true;
}

bool ss_intersection(sparse_set_ptr a, sparse_set_ptr b, sparse_set_ptr result)
{
    // Checking if pointers to sparse sets exist
    if(a == NULL || b == NULL || result == NULL)
    {
        printf("\nOne or more, of the given pointers is NULL.\n");
        return false;
    }

    if(ss_size(result) != 0)
    {
        printf("\nDestination Sparse Set is not empty!\n");
        return false;
    }

    if(ss_size(a) == 0 || ss_size(b) == 0)
        return true;

    SS_FOREACH(elem, b,
               if (ss_contained(a, elem)) {
                        if (!ss_add(result, elem)) {
                            printf("\nError adding %d.\n", elem);
                        }
                    })

    return true;
}

bool ss_difference(sparse_set_ptr a, sparse_set_ptr b, sparse_set_ptr result)
{
    // Checking if pointers to structs exist
    if(a == NULL || b == NULL || result == NULL)
    {
        printf("\nOne, or more, of the given pointers is NULL.\n");
        return false;
    }

    if(ss_size(result) != 0)
    {
        printf("\nDestination Sparse Set is not empty!\n");
        return false;
    }

    if(ss_size(b) == 0)
    {
        SS_FOREACH(elem, a,
                   if (!ss_add(result, elem)) {
                       printf("\nError adding %d.\n", elem);
                   }
        )
    } else {
        SS_FOREACH(elem, a,
                   if (!ss_contained(b, elem)) {
                       if (!ss_add(result, elem)) {
                           printf("\nError adding %d.\n", elem);
                       }
                   })
    }

    return true;
}

/**
 * Question 2D(i)
 */
bool ss_populate_from_text(const char *text, sparse_set_ptr result)
{
    if(result == NULL)
    {
        printf("\nGiven pointer to sparse set is NULL.\n");
        return false;
    }

    for(int i=0; i<result->capacity; i++)
    {
        ss_add(result, text[i]);
        if(text[i] == '\0') break;
    }

    return true;
}

/**
 * Question 2D(v)
 */

bool ss_populate_from_file(const char *filename, sparse_set_ptr result)
{
    FILE *fp;

    if(result == NULL)
    {
        printf("\nGiven pointer to sparse set is NULL.\n");
        return false;
    }

    if((fp = fopen(filename, "r")) == NULL)
    {
        fprintf(stderr, "\nFile %s not found.\nCheck if file path is correct.\n", filename);
        return false;
    }

    // Setting file position to end to calculate file size
    fseek(fp, 0, SEEK_END);
    int size = ftell(fp);

    bool to_return;

    // Validating file content
    if(size == 0)
    {
        printf("File is empty.\n");
        to_return = false;
    } else {
        // Resetting file pointer to beginning after validation
        rewind(fp);

        char temp_char;
        int i=0;

        while(i<result->capacity && ftell(fp) <= size)
        {
            fscanf(fp, "%c", &temp_char);
            ss_add(result, temp_char);

            i++;
        }

        ss_add(result, '\0');

        to_return = true;
    }

    if (fclose(fp) != 0)
        fputs("Error closing file.\n", stderr);

    return to_return;
}