#include <stdio.h>
#include <string.h>
#include "question_1.h"

#define MAX_LEN 999
#define PATH_LEN 100

int main(void)
{
    char text[MAX_LEN], file_path[PATH_LEN];
    statistics_t stats;
    menu_choice_t menu_choice;

    do{
        // For clearer output
        printf("\n");
        if(print_menu(&menu_choice))
        {
            switch (menu_choice) {
                case USER_INPUT:
                    // If error occurs go back to menu
                    if(!get_text_from_user(text, MAX_LEN))
                    {
                        fputs("\nError reading text.\n", stderr);
                        continue;
                    }
                    break;
                case FILE_INPUT:
                    printf("\nEnter full file path: ");
                    fgets(file_path, PATH_LEN, stdin);

                    // If given path is not valid, we go back to menu
                    if(!clean_and_validate(file_path))
                    {
                        fputs("\nInvalid file path.\n", stderr);
                        continue;
                    }

                    // Getting text from file
                    if(!get_text_from_file(file_path, text, MAX_LEN)) continue;

                    break;
                case QUIT:
                    printf("\nGood bye!\n");
                    // We go to the next iteration where condition will fail and hence the program will terminate
                    continue;
            }

            // Validating text
            if(!clean_and_validate(text))
            {
                printf("\nGiven text is invalid; empty or contains non-printable characters!\n");
                continue;
            }

            printf("\nYour text:\n%s\n", text);

            convert_to_lower_case(text, strlen(text));
            printf("\nYour text in lower case format:\n%s\n", text);

            get_text_statistics(text, strlen(text), &stats);

            // Printing Letter Frequencies
            print_frequencies(stats.freq);

            // Printing stats in table
            print_table(stats);
        }
        else
            fputs("\nInvalid menu option!\n", stderr);

        // For clearer output
        printf("\n");

    } while(menu_choice != QUIT);

    return 0;
}