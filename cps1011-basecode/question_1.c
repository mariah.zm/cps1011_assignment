#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "includes/question_1.h"

/**
 * Question 1A
 */

bool get_text_from_user(char *buf, size_t buf_size)
{
    printf("\nEnter a paragraph of text. Press ENTER when ready.\n\n-->");

    // On error a null pointer is returned - i.e. no success
    if(fgets(buf, buf_size, stdin) == NULL)
       return false;

    if(buf[strlen(buf)-1] != '\n')
        while(getchar() != '\n');

    return true;
}

bool clean_and_validate(char *str)
{
    size_t len = strlen(str);

    if(str[len-1] == '\n')
        str[len-1] = '\0';

    // If first character is null terminator, then it means no characters were entered
    if(str[0] == '\0')
        return false;

    int i=0;

    // Parsing string and returning false upon getting to a non printable character
    while(str[i] != '\0')
    {
        // Using isprint() instead of isgraph() since [space] is included
        if(!isprint(str[i]))
            return false;

        i++;
    }

    return true;
}

void convert_to_lower_case(char *str, size_t len)
{
    for(int i=0; i<len; i++)
        str[i] = tolower(str[i]);
}

/**
 * Question 1B
 */

int is_word_char(char ch)
{
    if(isalnum(ch) || ch == '-' || ch == '\'' || ch == '_')
        return 1;

    return 0;
}

int is_sentence_terminator(char ch)
{
    if(ch == '?' || ch == '!' || ch == '.')
        return 1;

    return 0;
}

int count_sentences(const char *text, size_t len)
{
    int count=0;

    // Skipping first character since that will be checked if word_char in first iteration
    for (int i=1; i<len; ++i) {
        if(is_sentence_terminator(text[i]))
            if(is_word_char(text[i-1]))
                count++;
    }

    return count;
}

void get_char_counts(const char *text, size_t len, char_counts_t *counts)
{
    counts->word = 0, counts->space = 0, counts->other = 0;

    for (int i=0; i<len; i++){
        if(is_word_char(text[i]))
            counts->word++;
        else if (isspace(text[i]))
            counts->space++;
        else
            counts->other++;
    }
}

int count_words(const char *text, size_t len)
{
    int count=0;

    for (int i=0; i<len; ++i) {
        if(is_word_char(text[i])){
            // If previous character was also a word_char, it means we're still parsing the same word
            if(i==0 || !is_word_char(text[i-1]))
                count++;
        }
    }

    return count;
}

void get_letter_frequencies(const char *text, size_t len, int freq[26], int *max_freq)
{
    // Initialising array and max_freq
    *max_freq=0;

    for(int i=0; i<26; i++)
        freq[i]=0;

    for(int j=0; j<len; j++){
        if(isalpha(text[j])) {
            freq[text[j] - 97]++;

            // New value is compared to max_freq to check if the variable needs updating
            if(freq[text[j]-97] > *max_freq)
                *max_freq = freq[text[j]-97];
        }
    }
}

void get_text_statistics(const char *text, size_t len, statistics_t *data)
{
    get_char_counts(text, len, &data->char_info);

    data->sentences = count_sentences(text, len);

    data->words = count_words(text, len);

    get_letter_frequencies(text, len, data->freq, &data->max_freq);

    // Index for max_freq_char string
    int j=0;

    for(int i=0; i<26; i++){
        if(data->freq[i] == data->max_freq){
            // 97 represents 'a' in ASCII
            data->most_freq_chars[j] = i+97;
            j++;
        }
    }

    //Setting last index as null character to indicate end of sequence
    data->most_freq_chars[j] = '\0';
}

/**
 * Question 1C
 */

bool get_text_from_file(const char* file_path, char *buf, size_t buf_size)
{
    FILE *fp;

    // If file is not found, false is returned
    if((fp = fopen(file_path, "r")) == NULL)
    {
        printf("File %s not found.\nCheck if file path is correct.\n", file_path);
        return false;
    }

    bool to_return = true;

    // fgets returns a NULL pointer if an error occurs during reading, therefore we return false
    if(fgets(buf, buf_size, fp) == NULL)
        to_return = false;

    if (fclose(fp) != 0)
        printf("Error closing file.\n");

    return to_return;
}

/**
 * Question 1D
 */

bool print_menu(menu_choice_t *user_choice)
{
    int menu_option;

    printf("---------MENU---------\n\n"
           "1. Enter text through user input\n"
           "2. Read text from file\n"
           "3. Quit\n\n"
           "Enter a menu option: ");

    scanf("%d", &menu_option);

    while(getchar() != '\n');

    switch (menu_option) {
        case 1:
            *user_choice = USER_INPUT;
            break;
        case 2:
            *user_choice = FILE_INPUT;
            break;
        case 3:
            *user_choice = QUIT;
            break;
        default:
            *user_choice = INVALID;
            return false;
    }

    return true;
}

/**
 * Question 1E
 */

void print_frequencies(int frequencies[26])
{
    printf("\nLetter Frequencies:\n");

    for(int i=0; i<26; i++)
    {
        if(frequencies[i] != 0)
            printf("[%c] : %3.d\n", 'a'+i, frequencies[i]);
    }
}

void print_table(statistics_t stats)
{
    // Printing stats in table

    int char_count = stats.char_info.word + stats.char_info.space + stats.char_info.other;

    printf("\n+--------------------------------+-------+\n"
           "| Feature                        | Value |\n"
           "+--------------------------------+-------+\n"
           "| Number of characters           | %5d |\n"
           "| Number of word characters      | %5d |\n"
           "| Number of spaces               | %5d |\n"
           "| Number of other characters     | %5d |\n"
           "| Number of words                | %5d |\n"
           "| Number of sentences            | %5d |\n"
           "| Maximum frequency              | %5d |\n"
           "| Most frequent letter(s)        |",
           char_count, stats.char_info.word, stats.char_info.space, stats.char_info.other,
           stats.words, stats.sentences, stats.max_freq);

    int index=0;

    while (stats.most_freq_chars[index] != '\0')
    {
        if(index == 0)
            printf("   [%c] |\n", stats.most_freq_chars[index]);
        else
            printf("|                                |   [%c] |\n", stats.most_freq_chars[index]);
        index++;
    }

    printf("+--------------------------------+-------+\n");
}
